// Auto Generated
using UnityEngine;
using UnityEditor;
using Libs.Nelios.Core;public static class GeneratedScenesList {
	[MenuItem("Nelios/Open Scene/_Dev/0", false, 11)]
	private static void Open__Dev_0() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/_Dev/0.unity");
	}
	[MenuItem("Nelios/Open Scene/_Dev/1", false, 11)]
	private static void Open__Dev_1() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/_Dev/1.unity");
	}
	[MenuItem("Nelios/Open Scene/_Dev/2", false, 11)]
	private static void Open__Dev_2() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/_Dev/2.unity");
	}
	[MenuItem("Nelios/Open Scene/_Dev/Dev_Mino", false, 11)]
	private static void Open__Dev_Dev_Mino() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/_Dev/Dev_Mino.unity");
	}
	[MenuItem("Nelios/Open Scene/_Dev/TilesSetup", false, 11)]
	private static void Open__Dev_TilesSetup() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/_Dev/TilesSetup.unity");
	}
	[MenuItem("Nelios/Open Scene/Common", false, 11)]
	private static void Open_Common() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Common.unity");
	}
	[MenuItem("Nelios/Open Scene/Elisa/Elisa_Lib", false, 11)]
	private static void Open_Elisa_Elisa_Lib() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Elisa/Elisa_Lib.unity");
	}
	[MenuItem("Nelios/Open Scene/Elisa/Map_Elisa_1", false, 11)]
	private static void Open_Elisa_Map_Elisa_1() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Elisa/Map_Elisa_1.unity");
	}
	[MenuItem("Nelios/Open Scene/Elisa/Map_Elisa_2", false, 11)]
	private static void Open_Elisa_Map_Elisa_2() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Elisa/Map_Elisa_2.unity");
	}
	[MenuItem("Nelios/Open Scene/Elisa/Map_Elisa_3", false, 11)]
	private static void Open_Elisa_Map_Elisa_3() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Elisa/Map_Elisa_3.unity");
	}
	[MenuItem("Nelios/Open Scene/Elisa/Map_Elisa_4", false, 11)]
	private static void Open_Elisa_Map_Elisa_4() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Elisa/Map_Elisa_4.unity");
	}
	[MenuItem("Nelios/Open Scene/Elisa/Test_Elisa", false, 11)]
	private static void Open_Elisa_Test_Elisa() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Elisa/Test_Elisa.unity");
	}
	[MenuItem("Nelios/Open Scene/Elisa/Test_Elisa2", false, 11)]
	private static void Open_Elisa_Test_Elisa2() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Elisa/Test_Elisa2.unity");
	}
	[MenuItem("Nelios/Open Scene/Florent/Flo_Lib", false, 11)]
	private static void Open_Florent_Flo_Lib() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Florent/Flo_Lib.unity");
	}
	[MenuItem("Nelios/Open Scene/Florent/Map_Flo_1", false, 11)]
	private static void Open_Florent_Map_Flo_1() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Florent/Map_Flo_1.unity");
	}
	[MenuItem("Nelios/Open Scene/Florent/Map_Flo_2", false, 11)]
	private static void Open_Florent_Map_Flo_2() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Florent/Map_Flo_2.unity");
	}
	[MenuItem("Nelios/Open Scene/Florent/Map_Flo_3", false, 11)]
	private static void Open_Florent_Map_Flo_3() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Florent/Map_Flo_3.unity");
	}
	[MenuItem("Nelios/Open Scene/Florent/Map_Flo_4", false, 11)]
	private static void Open_Florent_Map_Flo_4() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Florent/Map_Flo_4.unity");
	}
	[MenuItem("Nelios/Open Scene/GameLevel", false, 11)]
	private static void Open_GameLevel() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/GameLevel.unity");
	}
	[MenuItem("Nelios/Open Scene/GameOver", false, 11)]
	private static void Open_GameOver() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/GameOver.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_Base", false, 11)]
	private static void Open_LD_Sissi_LD_Base() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_Base.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_Fundation", false, 11)]
	private static void Open_LD_Sissi_LD_Fundation() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_Fundation.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_Screen_1", false, 11)]
	private static void Open_LD_Sissi_LD_Screen_1() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_Screen_1.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_Screen_2", false, 11)]
	private static void Open_LD_Sissi_LD_Screen_2() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_Screen_2.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_Screen_3", false, 11)]
	private static void Open_LD_Sissi_LD_Screen_3() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_Screen_3.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_Screen_4", false, 11)]
	private static void Open_LD_Sissi_LD_Screen_4() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_Screen_4.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_TL1", false, 11)]
	private static void Open_LD_Sissi_LD_TL1() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_TL1.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/LD_TopLeft_1", false, 11)]
	private static void Open_LD_Sissi_LD_TopLeft_1() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/LD_TopLeft_1.unity");
	}
	[MenuItem("Nelios/Open Scene/LD_Sissi/Sissi_Lib", false, 11)]
	private static void Open_LD_Sissi_Sissi_Lib() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/LD_Sissi/Sissi_Lib.unity");
	}
	[MenuItem("Nelios/Open Scene/Menu", false, 11)]
	private static void Open_Menu() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Menu.unity");
	}
	[MenuItem("Nelios/Open Scene/Tuto", false, 11)]
	private static void Open_Tuto() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Tuto.unity");
	}
	[MenuItem("Nelios/Open Scene/Victory", false, 11)]
	private static void Open_Victory() {
		SceneUtils.OpenScene("Assets/Resources/Scenes/Victory.unity");
	}
}
