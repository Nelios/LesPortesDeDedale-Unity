﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Libs.Nelios {
	public class ListScenes : Editor {
		private const string AssetPath = "Libs/Nelios/Editor/GeneratedScenesList.cs";
		private const string ScenesPath = "Resources/Scenes";

		[MenuItem("Nelios/Open Scene/Refresh Scenes", false, 0)]
		private static void RefreshScenes() {
			GenerateSceneMenuItems();
		}

		private static void GenerateSceneMenuItems() {
			var scriptPath = $"{Application.dataPath}/{AssetPath}";

			var scenes = Directory.GetFiles($"{Application.dataPath}/{ScenesPath}", "*.unity", SearchOption.AllDirectories);
			Array.Sort(scenes);

			var b = new StringBuilder();
			b.AppendFormat("// Auto Generated{0}using UnityEngine;{0}using UnityEditor;{0}using Libs.Nelios.Core;", Environment.NewLine);
			b.AppendLine("public static class GeneratedScenesList {");

			foreach (var scene in scenes) {
				var s = scene.Replace("\\", "/");
				var match = Regex.Match(s, "(Assets/Resources/Scenes/)(.*)(.[U-u]nity)");
				b.AppendLine($"\t[MenuItem(\"Nelios/Open Scene/{match.Groups[2]}\", false, 11)]");
				b.AppendLine($"\tprivate static void Open_{match.Groups[2].ToString().Replace(" ", "").Replace("/", "_")}() {{");
				b.AppendLine($"\t\tSceneUtils.OpenScene(\"{match.Groups[0]}\");");
				b.AppendLine("\t}");
			}
			b.AppendLine("}");

			File.Delete(scriptPath);
			File.WriteAllText(scriptPath, b.ToString(), Encoding.Default);

			AssetDatabase.ImportAsset($"Assets/{AssetPath}");
		}
	}
}
