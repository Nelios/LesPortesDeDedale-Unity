﻿using System;
using UnityEditor;
using System.Collections.Generic;
using Sources.Tiles;
using UnityEngine;
using System.Linq;

namespace Libs.Nelios {
	[CustomEditor(typeof(TileChooser))]
	[CanEditMultipleObjects]
	public class TileChooserEditor : Editor {
		private const string TilesPath = "Resources/Art/Tilesets/Maze_tileset.png";
		private TileChooser _go;
		private TileChooser[] _targets;
		private Sprite _selectedSprite;
		private Sprite[] _tiles;

		private void OnEnable() {
			_go = target as TileChooser;
		}

		public override void OnInspectorGUI() {
			// DrawDefaultInspector();

			Undo.RecordObject(_go, "Change tile");
			if (GUILayout.Button("Refresh Tiles")) {
				_tiles = AssetDatabase.LoadAllAssetRepresentationsAtPath($"Assets/{TilesPath}")
				.OfType<Sprite>().ToArray();
				_go.Tiles = new List<string>();
				foreach (var tile in _tiles) {
					_go.Tiles.Add(tile.name);
				}
			}

			if (_go.Tiles.Count > 0) {
				_go.Selected = EditorGUILayout.Popup("Tile", _go.Selected, _go.Tiles.ToArray());
				_go.GetComponent<SpriteRenderer>().sprite = Resources.LoadAll<Sprite>("Art/Tilesets/Maze_tileset")[_go.Selected];
			}
		}
	}
}