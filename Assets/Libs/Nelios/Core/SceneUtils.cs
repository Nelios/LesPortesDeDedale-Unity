﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Libs.Nelios.Core {
	public class SceneUtils {
		public static void OpenScene(string sceneFullPath) {
			if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
				EditorSceneManager.OpenScene(sceneFullPath);
			}
		}
	}
}
#endif