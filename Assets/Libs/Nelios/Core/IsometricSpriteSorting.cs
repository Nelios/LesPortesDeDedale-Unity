﻿using UnityEngine;

namespace Libs.Nelios.Core {
	[ExecuteInEditMode]
	public class IsometricSpriteSorting : MonoBehaviour {
		private Transform _transform;
		private SpriteRenderer _renderer;
		[SerializeField]
		private bool _constantUpdate = false;

		private void Awake() {
			_transform = GetComponent<Transform>();
			_renderer = GetComponent<SpriteRenderer>();
			if (Application.isPlaying && !_constantUpdate) {
				Destroy(this);
			}
		}

		private void Update() {
			_renderer.sortingOrder = (int) (_transform.position.y * -10);
		}
	}
}