﻿using UniRx;
using UniRx.Operators;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Sources.Controllable {
	public class ControllableInputState {
		public readonly ReactiveProperty<float> MovementX;
		public readonly ReactiveProperty<float> MovementY;

		public ControllableInputState() {
			MovementX = new ReactiveProperty<float>(.0f);
			MovementY = new ReactiveProperty<float>(.0f);
		}
	}
}