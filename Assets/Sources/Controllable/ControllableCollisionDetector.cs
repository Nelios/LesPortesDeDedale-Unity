﻿using System;
using UnityEngine;

namespace Sources.Controllable {
	public class ControllableCollisionDetector : MonoBehaviour {
		public void OnEnterCollision2D(Collider2D other) {
			Debug.Log("Collision !");
		}
	}
}

