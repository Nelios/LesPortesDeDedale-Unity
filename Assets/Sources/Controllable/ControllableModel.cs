﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace Sources.Controllable {
	public class ControllableModel {
		public ControllableModel(Rigidbody2D rigidbody2D, Transform transform) {
			Rigidbody2D = rigidbody2D;
			Transform = transform;
		}

		public Rigidbody2D Rigidbody2D { get; }
		public Transform Transform { get; }
		public Vector3 Position {
			get { return Transform.position; }
			set { Transform.position = value; }
		}

		public int MoveCounter = 2;
	}
}