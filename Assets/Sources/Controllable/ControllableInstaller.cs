﻿using System;
using UnityEngine;
using Zenject;

namespace Sources.Controllable {
	public class ControllableInstaller : MonoInstaller {
		public ControllableMoveHandler.Settings ControllableMoveHandlerSettings;

		public override void InstallBindings() {
			Container.Bind<ControllableModel>().AsSingle();
			Container.BindInstance(ControllableMoveHandlerSettings);

			Container.BindAllInterfaces<ControllableInputState>().To<ControllableInputState>().AsSingle();
			Container.BindAllInterfaces<ControllableInputHandler>().To<ControllableInputHandler>().AsSingle();
			Container.BindAllInterfaces<ControllableMoveHandler>().To<ControllableMoveHandler>().AsSingle();

			Container.Bind<ControllableInputState>().AsSingle();
		}
	}
}
