﻿using System;
using System.Collections;
using DG.Tweening;
using ModestTree;
using Sources.Camera;
using UniRx;
using UnityEngine;
using Zenject;

namespace Sources.Controllable {
	public class ControllableMoveHandler : IInitializable, ILateTickable {
		private readonly ControllableModel _controllable;
		private readonly ControllableInputState _controllableInputState;
		private readonly PixelPerfectCamera _pixelPerfectCamera;
		private readonly Settings _settings;
		private bool _canMove = true;
		private Vector2 _targetPos;
		private Vector3 _roundedPosition;
		private Vector2 _direction;
		private Vector3 _target = new Vector3(0, 0, 0);

		public ControllableMoveHandler(ControllableModel controllable,
									   ControllableInputState controllableInputState,
									   PixelPerfectCamera pixelPerfectCamera,
									   Settings settings) {
			_controllable = controllable;
			_controllableInputState = controllableInputState;
			_pixelPerfectCamera = pixelPerfectCamera;
			_settings = settings;
		}

		public void Initialize() {
			_controllableInputState.MovementX.Subscribe(
				movementX => {
					Observable.FromCoroutine(_ => MoveX(_controllable, movementX)).Subscribe();
				}
			);

			_controllableInputState.MovementY.Subscribe(
				movementY => {
					Observable.FromCoroutine(_ => MoveY(_controllable, movementY)).Subscribe();
				}
			);
		}

		private IEnumerator MoveX(ControllableModel controllable, float movementX) {
			_direction.x = movementX;
			yield return null;
		}

		private IEnumerator MoveY(ControllableModel controllable, float movementY) {
			_direction.y = movementY;
			yield return null;
		}

		private Vector3 GetRoundedPosition() {
			if (_direction != Vector2.zero) {
				_target.z = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg - 90f;
				_controllable.Transform.rotation = Quaternion.Euler(_target);
			}

			_direction.Normalize();
			_controllable.Rigidbody2D.velocity = _direction * _settings.PlayerSpeed;

			_roundedPosition.x = Mathf.Round(_controllable.Position.x * _pixelPerfectCamera.GetPixelPerUnit) / _pixelPerfectCamera.GetPixelPerUnit;
			_roundedPosition.y = Mathf.Round(_controllable.Position.y * _pixelPerfectCamera.GetPixelPerUnit) / _pixelPerfectCamera.GetPixelPerUnit;
			_roundedPosition.z = Mathf.Round(_controllable.Position.z * _pixelPerfectCamera.GetPixelPerUnit) / _pixelPerfectCamera.GetPixelPerUnit;

			return _roundedPosition;
		}

		public void LateTick() {
			_controllable.Position = GetRoundedPosition();
		}

		[Serializable]
		public class Settings {
			public float PlayerSpeed;
		}
	}
}