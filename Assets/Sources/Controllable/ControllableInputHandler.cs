﻿using UnityEngine;
using Zenject;
using UniRx;

namespace Sources.Controllable {
	public class ControllableInputHandler : ITickable {
		private readonly ControllableInputState _controllableInputState;
		private int _player1Input = 1;
		private int _player2Input = 2;

		public ControllableInputHandler(ControllableInputState controllableInputState) {
			_controllableInputState = controllableInputState;
		}

		public void Tick() {
			if (Input.GetAxisRaw("Horizontal_1") > 0) {
				_controllableInputState.MovementX.Value = Input.GetAxisRaw("Horizontal_1");
			} else if (Input.GetAxisRaw("Horizontal_1") < 0) {
				_controllableInputState.MovementX.Value = Input.GetAxisRaw("Horizontal_1");
			} else {
				_controllableInputState.MovementX.Value = 0.0f;
			}

			if (Input.GetAxisRaw("Vertical_1") > 0) {
				_controllableInputState.MovementY.Value = Input.GetAxisRaw("Vertical_1");
			} else if (Input.GetAxisRaw("Vertical_1") < 0) {
				_controllableInputState.MovementY.Value = Input.GetAxisRaw("Vertical_1");
			} else {
				_controllableInputState.MovementY.Value = 0.0f;
			}
		}
	}
}