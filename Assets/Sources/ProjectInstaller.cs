﻿using Sources.Camera;
using Zenject;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sources {
	public class ProjectInstaller : MonoInstaller {
		public MainCamera.Settings MainCameraSettings;

		public override void InstallBindings() {
			Container.BindInstance(MainCameraSettings);
			Container.Bind<PixelPerfectCamera>().AsSingle();
			Container.BindAllInterfaces<MainCamera>().To<MainCamera>().AsSingle();
		}
	}
}
