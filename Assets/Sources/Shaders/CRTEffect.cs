﻿using UnityEngine;

namespace Sources.Shaders {
	[ExecuteInEditMode]
	public class CRTEffect : MonoBehaviour {
		private Material _material;
		[Range(-16.0f, 0.0f)]
		public float HardScan = -8.0f;
		[Range(-16.0f, 0.0f)]
		public float HardPix = -3.0f;
		[Range(0.0f, 2.0f)]
		public float MaskDark = 0.5f;
		[Range(0.0f, 2.0f)]
		public float MaskLight = 1.5f;
		public Vector4 Warp = new Vector4(0.03125f, 0.04166f, 0, 0);
		[Range(1.0f, 16.0f)]
		public float ResolutionScale = 4.0f;

		private void Awake() {
			_material = new Material(Shader.Find("Nelios/CRT"));
		}

		private void OnRenderImage(RenderTexture source, RenderTexture destination) {
			if (_material != null) {
				_material.SetFloat("hardScan", HardScan);
				_material.SetFloat("hardPix", HardPix);
				_material.SetFloat("maskDark", MaskDark);
				_material.SetFloat("maskLight", MaskLight);
				_material.SetVector("warp", Warp);
				_material.SetFloat("resScale", ResolutionScale);
				Graphics.Blit(source, destination, _material);
			} else
				Graphics.Blit(source, destination);
		}
	}
}