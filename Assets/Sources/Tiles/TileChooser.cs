﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Sources.Tiles {
	public class TileChooser : MonoBehaviour {
		public List<string> Tiles;
		public int Selected;
	}
}