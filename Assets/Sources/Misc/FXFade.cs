﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Assets.Sources.Misc {
	public class FXFade : MonoBehaviour {
		private SpriteRenderer _sprite;

		private void Awake() {
			_sprite = GetComponent<SpriteRenderer>();
		}

		public void ShowFX() {
			StartCoroutine(FadeFX());
		}

		private IEnumerator FadeFX() {
			_sprite.DOFade(1f, 0f);
			yield return new WaitForSeconds(1f);
			_sprite.DOFade(0f, 2f);
		}
	}
}