﻿using UnityEngine;

namespace Assets.Sources.Misc {
	public class AutoRotateButton : MonoBehaviour {
		public bool RotateEveryFrame;

		private void Awake() {
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}

		private void Update() {
			if (RotateEveryFrame)
				transform.rotation = Quaternion.Euler(0, 0, 0);
		}
	}
}