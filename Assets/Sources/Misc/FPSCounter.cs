﻿using UnityEngine;
using System.Collections;
using Sources.AI;

namespace Sources.Misc {
	public class FPSCounter : MonoBehaviour {
		private float _deltaTime = 0.0f;
		public MinoAI MinoAI;
		private bool _hideFPS = true;

		void Update() {
			_deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;

			if (Input.GetKeyDown("m"))
				_hideFPS = !_hideFPS;
		}

		void OnGUI() {
			if (_hideFPS)
				return;
			int w = Screen.width, h = Screen.height;

			var style = new GUIStyle();

			var rect = new Rect(0, 0, w, h * 2f / 100f);
			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 5 / 100;
			style.normal.textColor = new Color(0.9f, 0.7f, 0.5f, 1.0f);
			var msec = _deltaTime * 1000.0f;
			var fps = 1.0f / _deltaTime;
			var text = $"{msec:0.0} ms ({fps:0.} fps.) {MinoAI.HP} HP";
			GUI.Label(rect, text, style);
		}
	}
}