﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Sources.Misc {
	public class GameOver : MonoBehaviour {
		private bool GoToMenu = false;

		private IEnumerator FadeToMenu() {
			var cameraFader = GameObject.Find("Main Camera").GetComponent<ScreenFader>();
			cameraFader.FadeToBlack();
			yield return new WaitForSeconds(cameraFader.FadeSpeed);
			SceneManager.LoadScene("Menu");
		}

		private void Update() {
			if (GoToMenu)
				return;
			if (!Input.GetButtonDown("Green") && !Input.GetButtonDown("Red") && !Input.GetButtonDown("Blue") && !Input.GetButtonDown("Yellow"))
				return;

			GoToMenu = true;
			StartCoroutine(FadeToMenu());
		}
	}
}