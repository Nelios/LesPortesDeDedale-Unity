﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using UnityEngine;

namespace Assets.Sources.Misc {
	public class PlayerHP : MonoBehaviour {
		public int HP = 3;
		public SpriteRenderer _sprite;
		public Animator FXAnimator;

		public void ShowFX() {
			StartCoroutine(FadeFX());
		}

		private IEnumerator FadeFX() {
			_sprite.enabled = true;
			FXAnimator.SetTrigger("Hit");
			yield return new WaitForSeconds(.4f);
			_sprite.enabled = false;
		}
	}
}
