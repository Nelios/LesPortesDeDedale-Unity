﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Sources.Misc {
	public class Tuto : MonoBehaviour {
		public GameObject[] TutoScreens; 
		private int _tutoNumber = 0;

		private void Awake() {
			TutoScreens[0].SetActive(true);
		}

		private IEnumerator FadeToMenu() {
			var cameraFader = GameObject.Find("Main Camera").GetComponent<ScreenFader>();
			cameraFader.FadeToBlack();
			yield return new WaitForSeconds(cameraFader.FadeSpeed);
			SceneManager.LoadScene("GameLevel");
		}

		public void DisableEverything(GameObject[] gos, int except) {
			foreach (var go in gos) {
				go.SetActive(false);
			}
			gos[except].SetActive(true);
		}

		private void Update() {
			if (!Input.GetButtonDown("Green") && !Input.GetButtonDown("Red") && !Input.GetButtonDown("Blue") && !Input.GetButtonDown("Yellow"))
				return;
			if (_tutoNumber == TutoScreens.Length - 1) {
				StartCoroutine(FadeToMenu());
				return;
			}

			_tutoNumber += 1;
			DisableEverything(TutoScreens, _tutoNumber);
		}
	}
}