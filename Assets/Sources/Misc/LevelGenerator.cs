﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

namespace Assets.Sources.Misc {
	public class LevelGenerator : MonoBehaviour {
		private int[] _maps = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		private Vector3[] _mapsPosition = { new Vector3(-15.5f, 8.5f, 0),
			new Vector3(15.5f, 8.5f, 0),
			new Vector3(-15.5f, -8.5f, 0),
			new Vector3(15.5f, -8.5f, 0) };
		private readonly Random _rng = new Random();
		private Transform _controllable;
		private Transform _camera;
		private Transform _mino;

		private void Awake() {
			_maps = _maps.OrderBy(x => _rng.Next()).ToArray();
			// SceneManager.LoadSceneAsync("0", LoadSceneMode.Additive);
			for (var i = 0; i < 4; i++) {
				SceneManager.LoadSceneAsync(_maps[i].ToString(), LoadSceneMode.Additive);
			}

			_controllable = GameObject.Find("Controllable").GetComponent<Transform>();
			_camera = GameObject.Find("Main Camera").GetComponent<Transform>();
			_mino = GameObject.Find("Mino").GetComponent<Transform>();

			_mino.GetComponent<AILerp>().canSearch = false;
			_mino.GetComponent<AILerp>().canMove = false;
		}

		private void Start() {
			for (var i = 0; i < 4; i++) {
				var gos = SceneManager.GetSceneByName(_maps[i].ToString()).GetRootGameObjects();
				gos[0].transform.position = _mapsPosition[i];
			}

			var playerStartMap = _rng.Next(0, 3);

			switch (playerStartMap) {
			case 0:
				if (_rng.Next(0, 1) > 0) {
					MoveControllableAndCamera(-16f, 0f);
					_mino.position = new Vector3(0f, 8f, _mino.position.z);
					break;
				}
				MoveControllableAndCamera(0f, 8f);
				_mino.position = new Vector3(-16f, 0f, _mino.position.z);
				break;
			case 1:
				if (_rng.Next(0, 1) > 0) {
					MoveControllableAndCamera(15f, 0f);
					_mino.position = new Vector3(0f, 8f, _mino.position.z);
					break;
				}
				MoveControllableAndCamera(0f, 8f);
				_mino.position = new Vector3(15f, 0f, _mino.position.z);
				break;
			case 2:
				if (_rng.Next(0, 1) > 0) {
					MoveControllableAndCamera(-16f, 0f);
					_mino.position = new Vector3(0f, -8f, _mino.position.z);
					break;
				}
				MoveControllableAndCamera(0f, -9f);
				_mino.position = new Vector3(-16f, 0f, _mino.position.z);
				break;
			case 3:
				if (_rng.Next(0, 1) > 0) {
					MoveControllableAndCamera(15f, 0f);
					_mino.position = new Vector3(0f, -9f, _mino.position.z);
					break;
				}
				MoveControllableAndCamera(0f, -9f);
				_mino.position = new Vector3(15f, 0f, _mino.position.z);
				break;
			default:
				break;
			}

			_mino.GetComponent<AILerp>().canSearch = true;
			_mino.GetComponent<AILerp>().canMove = true;
		}

		private void MoveControllableAndCamera(float x, float y) {
			_controllable.position = new Vector3(x, y, _controllable.position.z);
			_camera.position = new Vector3(x, y, _camera.position.z);
		}

		private void Update() {
			if (Input.GetButtonDown("Back")) {
				SceneManager.LoadScene("Menu");
			}
		}
	}
}