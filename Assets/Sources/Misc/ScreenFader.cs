﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Sources.Misc {
	public class ScreenFader : MonoBehaviour {
		public Image FadeImage;
		public float FadeSpeed = 1.5f;

		private void Awake() {
			FadeImage.color = Color.black;
			FadeImage.DOColor(Color.clear, FadeSpeed);
			Cursor.visible = false;
		}

		public void FadeToBlack() {
			FadeImage.DOColor(Color.black, FadeSpeed);
		}
	}
}