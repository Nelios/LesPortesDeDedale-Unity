﻿using System;
using System.Collections;
using Assets.Sources.Misc;
using Sources.AI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sources.Elements {
	public class Spikes : MonoBehaviour {
		private Animator _animator;
		private MinoAI _minoAI;
		public GameObject TrapLight;
		private AILerp _minoAiLerp;
		public Animator TrapAnimator;
		public bool Ready = true;

		private void Awake() {
			_animator = GetComponent<Animator>();
			_minoAI = GameObject.Find("Mino").GetComponent<MinoAI>();
			_minoAiLerp = _minoAI.GetComponent<AILerp>();
		}

		private void HitMino() {
			_minoAI.HP -= 1;
			_minoAI.FXFade.ShowFX();

			if (_minoAI.HP == 0) {
				StartCoroutine(Victory());
			}

			StartCoroutine(DisableSpikes());
		}

		private IEnumerator Victory() {
			var cameraFader = GameObject.Find("Main Camera").GetComponent<ScreenFader>();
			cameraFader.FadeToBlack();
			yield return new WaitForSeconds(cameraFader.FadeSpeed);
			SceneManager.LoadScene("Victory");
		}

		private IEnumerator FreezeMino() {
			_minoAiLerp.speed = 0.1f;
			_minoAiLerp.canSearch = false;
			yield return new WaitForSeconds(0.5f);
			_minoAiLerp.speed = 1.6f;
			_minoAiLerp.canSearch = true;
		}

		private IEnumerator DisableSpikes() {
			yield return new WaitForSeconds(0.2f);
			TrapAnimator.SetTrigger("DisableTrap");
			TrapLight.SetActive(false);
			_animator.SetTrigger("DisableSpikes");
		}
	}
}