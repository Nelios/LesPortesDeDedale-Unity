﻿using System;
using System.Collections;
using Assets.Sources.Misc;
using Sources.Shaders;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

namespace Sources.Elements {
	public enum MenuState {
		TitleScreen,
		Menu,
		Options,
		Credits
	}

	public class Menu : MonoBehaviour {
		public ScreenOverlay GammaCorrection;
		public bool PostFX = true;
		public GameObject Titre_1;
		public GameObject Titre_2;
		public GameObject MainMenuGroup;
		public GameObject OptionsGroup;
		public GameObject CreditsGroup;
		public GameObject[] TitleButtons;
		public GameObject[] OptionsButtons;
		public GameObject[] PostFXOnOff;
		public MenuState MenuState;
		public int TitleIndex = 0;
		public int OptionsIndex = 0;
		public bool CanMove = true;
		public bool CanToggle = true;
		public bool LoadingLevel = false;
		private GameObject _camera;

		public void Awake() {
			_camera = GameObject.Find("Main Camera");
			if (PlayerPrefs.GetFloat("GammaCorrection") == 0.0f)
				PlayerPrefs.SetFloat("GammaCorrection", 2.0f);
			GammaCorrection.intensity = PlayerPrefs.GetFloat("GammaCorrection");
			PostFX = PlayerPrefs.GetInt("PostFX", 1) > 0;
			Titre_2.SetActive(false);
			Titre_1.SetActive(true);
			MainMenuGroup.SetActive(false);
			OptionsGroup.SetActive(false);
			TitleButtons[TitleIndex].SetActive(true);

			PostFXOnOff[PostFX ? 1 : 0].SetActive(true);
			PostFXOnOff[!PostFX ? 1 : 0].SetActive(false);
		}

		public void DisableEverything(GameObject[] gos, int except) {
			foreach (var go in gos) {
				go.SetActive(false);
			}
			gos[except].SetActive(true);
		}

		private IEnumerator FadeToGame() {
			LoadingLevel = true;
			var cameraFader = GameObject.Find("Main Camera").GetComponent<ScreenFader>();
			cameraFader.FadeToBlack();
			yield return new WaitForSeconds(cameraFader.FadeSpeed);
			SceneManager.LoadScene("Tuto");
		}

		public void Update() {
			if (LoadingLevel)
				return;

			if (Input.GetButtonDown("Green")) {
				switch (MenuState) {
				case MenuState.TitleScreen:
					Titre_1.SetActive(false);
					Titre_2.SetActive(true);
					MainMenuGroup.SetActive(true);
					DisableEverything(TitleButtons, TitleIndex);
					MenuState = MenuState.Menu;
					break;
				case MenuState.Menu:
					switch (TitleIndex) {
					case 0: // Jouer
						TitleButtons[0].SetActive(false);
						StartCoroutine(FadeToGame());
						break;
					case 1: // Options
						DisableEverything(OptionsButtons, OptionsIndex);
						OptionsGroup.SetActive(true);
						MainMenuGroup.SetActive(false);
						MenuState = MenuState.Options;
						break;
					case 2: // Credits
						CreditsGroup.SetActive(true);
						MainMenuGroup.SetActive(false);
						MenuState = MenuState.Credits;
						break;
					case 3: // Quit
						Application.Quit();
						Debug.Log("Quit!");
						break;
					}
					break;
				case MenuState.Options:
					PlayerPrefs.SetFloat("GammaCorrection", GammaCorrection.intensity);
					PlayerPrefs.SetInt("PostFX", PostFX ? 1 : 0);
					OptionsGroup.SetActive(false);
					MainMenuGroup.SetActive(true);
					MenuState = MenuState.Menu;
					break;
				case MenuState.Credits:
					CreditsGroup.SetActive(false);
					MainMenuGroup.SetActive(true);
					MenuState = MenuState.Menu;
					break;
				default:
					throw new ArgumentOutOfRangeException();
				}
			}

			if (Input.GetButtonDown("Blue")) {
				switch (MenuState) {
				case MenuState.Options:
					switch (OptionsIndex) {
					case 0:
						PostFX = true;
						PlayerPrefs.SetInt("PostFX", PostFX ? 1 : 0);
						_camera.GetComponent<NoiseAndGrain>().enabled = PostFX;
						_camera.GetComponent<CRTEffect>().enabled = PostFX;
						_camera.GetComponent<VignetteAndChromaticAberration>().enabled = PostFX;
						PostFXOnOff[0].SetActive(false);
						PostFXOnOff[1].SetActive(true);
						break;
					case 1:
						GammaCorrection.intensity = 2.0f;
						PlayerPrefs.SetFloat("GammaCorrection", GammaCorrection.intensity);
						break;
					}
					break;
				case MenuState.TitleScreen:
				case MenuState.Menu:
				case MenuState.Credits:
				default:
					break;
				}
			}

			if (Input.GetButtonDown("Red")) {
				switch (MenuState) {
				case MenuState.TitleScreen:
					break;
				case MenuState.Menu:
					Titre_2.SetActive(false);
					Titre_1.SetActive(true);
					MenuState = MenuState.TitleScreen;
					break;
				case MenuState.Options:
				case MenuState.Credits:
					GammaCorrection.intensity = PlayerPrefs.GetFloat("GammaCorrection");
					PostFX = PlayerPrefs.GetInt("PostFX", 1) > 0;
					CreditsGroup.SetActive(false);
					OptionsGroup.SetActive(false);
					MainMenuGroup.SetActive(true);
					MenuState = MenuState.Menu;
					break;
				default:
					throw new ArgumentOutOfRangeException();
				}
			}

			switch (MenuState) {
			case MenuState.Menu:
				if (Input.GetAxisRaw("Vertical_1") > 0.5f && CanMove) {
					if (TitleIndex == 0) {
						TitleIndex = TitleButtons.Length - 1;
					} else {
						TitleIndex -= 1;
					}
					DisableEverything(TitleButtons, TitleIndex);
					CanMove = false;
				} else if (Input.GetAxisRaw("Vertical_1") < -0.5f && CanMove) {
					if (TitleIndex == TitleButtons.Length - 1) {
						TitleIndex = 0;
					} else {
						TitleIndex += 1;
					}
					DisableEverything(TitleButtons, TitleIndex);
					CanMove = false;
				} else if (Input.GetAxisRaw("Vertical_1") == 0) {
					CanMove = true;
				}
				break;
			case MenuState.Options:
				if (Input.GetAxisRaw("Vertical_1") > 0.5f && CanMove) {
					if (OptionsIndex == 0) {
						OptionsIndex = OptionsButtons.Length - 1;
					} else {
						OptionsIndex -= 1;
					}
					DisableEverything(OptionsButtons, OptionsIndex);
					CanMove = false;
				} else if (Input.GetAxisRaw("Vertical_1") < -0.5f && CanMove) {
					if (OptionsIndex == OptionsButtons.Length - 1) {
						OptionsIndex = 0;
					} else {
						OptionsIndex += 1;
					}
					DisableEverything(OptionsButtons, OptionsIndex);
					CanMove = false;
				} else if (Input.GetAxisRaw("Vertical_1") == 0) {
					CanMove = true;
				}

				switch (OptionsIndex) {
				case 0:
					if (!CanToggle) {
						if (Input.GetAxisRaw("Horizontal_1") == 0)
							CanToggle = true;
						break;
					}

					if (Input.GetAxisRaw("Horizontal_1") > 0.5f || Input.GetAxisRaw("Horizontal_1") < -0.5f) {
						PostFX = !PostFX;
						foreach (var g in PostFXOnOff) {
							g.SetActive(!g.activeSelf);
						}
						CanToggle = false;
						_camera.GetComponent<NoiseAndGrain>().enabled = PostFX;
						_camera.GetComponent<CRTEffect>().enabled = PostFX;
						_camera.GetComponent<VignetteAndChromaticAberration>().enabled = PostFX;
					}
					break;
				case 1:
					if (Input.GetAxisRaw("Horizontal_1") > 0.5f) {
						if (GammaCorrection.intensity < 4.0f)
							GammaCorrection.intensity += 0.1f;
					} else if (Input.GetAxisRaw("Horizontal_1") < -0.5f) {
						if (GammaCorrection.intensity > 1.0f)
							GammaCorrection.intensity -= 0.1f;
					}
					break;
				default:
					break;
				}
				break;
			case MenuState.TitleScreen:
				break;
			case MenuState.Credits:
				break;
			default:
				throw new ArgumentOutOfRangeException();
			}
		}
	}
}