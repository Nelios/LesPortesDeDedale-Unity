﻿// SHUFFLE UNE LISTE ESPECE DE RETARD.

using System;
using System.Collections;
using UnityEngine;

namespace Sources.Elements {
	public enum Color {
		Green,
		Blue,
		Red,
		Yellow
	}

	public class Piege : MonoBehaviour {
		private Transform _transform;
		private Animator _animator;
		private bool _alreadyShot;
		public Animator SpikesAnimator;
		public Animator HUD;
		public SpriteRenderer[] Buttons;
		public Color Color_1;
		public Color Color_2;
		public Color Color_3;
		public ProjectInstaller _installer;
		public bool Ready;
		public int TrapCounter = 0;
		public bool CanInputTrap = true;
		public Light Light;
		public bool IsOnTrap = false;
		public SpriteRenderer BrokenTrap;

		private void Awake() {
			_animator = GetComponent<Animator>();
			_installer = GameObject.Find("SceneContext").GetComponent<ProjectInstaller>();
			_transform = GetComponent<Transform>();
			switch (Color_1) {
			case Color.Green:
				Buttons[0].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_A");
				break;
			case Color.Red:
				Buttons[0].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_B");
				break;
			case Color.Blue:
				Buttons[0].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_X");
				break;
			case Color.Yellow:
				Buttons[0].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_Y");
				break;
			}

			switch (Color_2) {
			case Color.Green:
				Buttons[1].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_A");
				break;
			case Color.Red:
				Buttons[1].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_B");
				break;
			case Color.Blue:
				Buttons[1].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_X");
				break;
			case Color.Yellow:
				Buttons[1].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_Y");
				break;
			}

			switch (Color_3) {
			case Color.Green:
				Buttons[2].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_A");
				break;
			case Color.Red:
				Buttons[2].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_B");
				break;
			case Color.Blue:
				Buttons[2].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_X");
				break;
			case Color.Yellow:
				Buttons[2].sprite = Resources.Load<Sprite>("Art/GUI/HUD/Bouton_Y");
				break;
			}
		}

		private void OnTriggerEnter2D(Collider2D collider) {
			if (_alreadyShot)
				return;
			if (collider.name == "Controllable")
				IsOnTrap = true;
			if (collider.name != "Controllable" || Ready)
				return;
			HUD.gameObject.SetActive(true);
			_installer.MainCameraSettings.CanOpenDoor = false;
		}

		private void OnTriggerStay2D(Collider2D collider) {
			if (collider.name != "Mino" || !Ready)
				return;
			if (!((_transform.position - collider.transform.position).magnitude < 0.8f))
				return;
			_animator.enabled = false;
			SpikesAnimator.SetTrigger("ActivateSpikes");
			_alreadyShot = true;
			GetComponent<SpriteRenderer>().sprite = BrokenTrap.sprite;
			Ready = false;
		}

		private void OnTriggerExit2D(Collider2D collider) {
			if (collider.name != "Controllable")
				return;
			IsOnTrap = false;
			HUD.gameObject.SetActive(false);
			_installer.MainCameraSettings.CanOpenDoor = true;
		}

		private void ActivateSpikes() {
			_animator.SetTrigger("EnableTrap");
			Light.gameObject.SetActive(true);
			SpikesAnimator.SetTrigger("EnableSpikes");
		}

		private void Update() {
			if (_alreadyShot)
				return;
			if (!CanInputTrap &&
				!Input.GetButton("Green") &&
				!Input.GetButton("Red") &&
				!Input.GetButton("Blue") &&
				!Input.GetButton("Yellow")) {
				CanInputTrap = true;
			}

			switch (TrapCounter) {
			case -1:
				Buttons[0].gameObject.SetActive(false);
				Buttons[1].gameObject.SetActive(false);
				Buttons[2].gameObject.SetActive(false);
				break;
			case 0:
				Buttons[0].gameObject.SetActive(true);
				Buttons[1].gameObject.SetActive(false);
				Buttons[2].gameObject.SetActive(false);
				break;
			case 1:
				Buttons[0].gameObject.SetActive(true);
				Buttons[1].gameObject.SetActive(true);
				Buttons[2].gameObject.SetActive(false);
				break;
			case 2:
				Buttons[0].gameObject.SetActive(true);
				Buttons[1].gameObject.SetActive(true);
				Buttons[2].gameObject.SetActive(true);
				break;
			case 3:
				Ready = true;
				ActivateSpikes();
				TrapCounter = 0;
				HUD.gameObject.SetActive(false);
				_installer.MainCameraSettings.CanOpenDoor = true;
				break;
			}

			if (!IsOnTrap)
				return;
			if (!CanInputTrap)
				return;
			if (TrapCounter == 2) {
				switch (Color_3) {
				case Color.Green:
					if (Input.GetButtonDown("Green")) {
						TrapCounter = 3;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Yellow")) {
						ResetTrap();
					}
					break;
				case Color.Red:
					if (Input.GetButtonDown("Red")) {
						TrapCounter = 3;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Green") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Yellow")) {
						ResetTrap();
					}
					break;
				case Color.Blue:
					if (Input.GetButtonDown("Blue")) {
						TrapCounter = 3;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Green") || Input.GetButtonDown("Yellow")) {
						ResetTrap();
					}
					break;
				case Color.Yellow:
					if (Input.GetButtonDown("Yellow")) {
						TrapCounter = 3;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Green")) {
						ResetTrap();
					}
					break;
				}
			}

			if (TrapCounter == 1) {
				switch (Color_2) {
				case Color.Green:
					if (Input.GetButtonDown("Green")) {
						TrapCounter = 2;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Yellow")) {
						ResetTrap();
					}
					break;
				case Color.Red:
					if (Input.GetButtonDown("Red")) {
						TrapCounter = 2;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Green") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Yellow")) {
						ResetTrap();
					}
					break;
				case Color.Blue:
					if (Input.GetButtonDown("Blue")) {
						TrapCounter = 2;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Green") || Input.GetButtonDown("Yellow")) {
						ResetTrap();
					}
					break;
				case Color.Yellow:
					if (Input.GetButtonDown("Yellow")) {
						TrapCounter = 2;
						CanInputTrap = false;
					} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Green")) {
						ResetTrap();
					}
					break;
				}
			}

			if (TrapCounter != 0)
				return;
			switch (Color_1) {
			case Color.Green:
				if (Input.GetButtonDown("Green")) {
					TrapCounter = 1;
					CanInputTrap = false;
				} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Yellow")) {
					ResetTrap();
				}
				break;
			case Color.Red:
				if (Input.GetButtonDown("Red")) {
					TrapCounter = 1;
					CanInputTrap = false;
				} else if (Input.GetButtonDown("Green") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Yellow")) {
					ResetTrap();
				}
				break;
			case Color.Blue:
				if (Input.GetButtonDown("Blue")) {
					TrapCounter = 1;
					CanInputTrap = false;
				} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Green") || Input.GetButtonDown("Yellow")) {
					ResetTrap();
				}
				break;
			case Color.Yellow:
				if (Input.GetButtonDown("Yellow")) {
					TrapCounter = 1;
					CanInputTrap = false;
				} else if (Input.GetButtonDown("Red") || Input.GetButtonDown("Blue") || Input.GetButtonDown("Green")) {
					ResetTrap();
				}
				break;
			}
		}

		private void ResetTrap() {
			TrapCounter = -1;
			CanInputTrap = false;
			StartCoroutine(Delay());
		}

		private IEnumerator Delay() {
			yield return new WaitForSeconds(0.5f);
			TrapCounter = 0;
		}
	}
}