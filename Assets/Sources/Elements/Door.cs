﻿using System;
using UnityEngine;

namespace Sources.Elements {
	public enum DoorState {
		Opened,
		Closed
	}

	public enum DoorColor {
		Green,
		Red,
		Blue,
		Yellow
	}

	public class Door : MonoBehaviour {
		public DoorState DoorState;
		public DoorColor DoorColor;
		public ProjectInstaller _installer;
		private Animator _animator;
		private BoxCollider2D _collider;

		private void Awake() {
			_installer = GameObject.Find("SceneContext").GetComponent<ProjectInstaller>();
			_animator = GetComponent<Animator>();
			_collider = GetComponent<BoxCollider2D>();
			switch (DoorState) {
				case DoorState.Closed:
					_animator.SetTrigger("Close");
					DoorState = DoorState.Closed;
					break;
				case DoorState.Opened:
					_animator.SetTrigger("Open");
					DoorState = DoorState.Opened;
					break;
			}
		}

		public void DoorOpened() {
			_collider.enabled = false;
			AstarPath.active.Scan();
		}

		public void DoorClosed() {
			_collider.enabled = true;
			AstarPath.active.Scan();
		}

		private void ToggleDoor(string Color) {
			if (!_installer.MainCameraSettings.CanOpenDoor) {
				return;
			}

			if (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1) {
				return;
			}

			if (!Input.GetButtonDown(Color))
				return;
			switch (DoorState) {
				case DoorState.Closed:
					_animator.SetTrigger("Open");
					DoorState = DoorState.Opened;
					break;
				case DoorState.Opened:
					_animator.SetTrigger("Close");
					DoorState = DoorState.Closed;
					break;
			}
		}

		private void Update() {
			switch (DoorColor) {
				case DoorColor.Green:
					ToggleDoor("Green");
					break;
				case DoorColor.Red:
					ToggleDoor("Red");
					break;
				case DoorColor.Blue:
					ToggleDoor("Blue");
					break;
				case DoorColor.Yellow:
					ToggleDoor("Yellow");
					break;
			}
		}
	}
}