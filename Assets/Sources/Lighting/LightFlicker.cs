﻿using System;
using UnityEngine;

namespace Sources.Lighting {
	public class LightFlicker : MonoBehaviour {
		private Light _light;
		private float _oldIntensity;
		public float MaxFlicker = 0.1f;
		public float MinFlicker = -0.1f;

		private void Awake() {
			_light = GetComponent<Light>();
			_oldIntensity = _light.intensity;
		}

		private void Update() {
			// _light.intensity = _oldIntensity + UnityEngine.Random.Range(MinFlicker, MaxFlicker);
		}
	}
}