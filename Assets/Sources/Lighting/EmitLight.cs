﻿using System.Collections.Generic;
using UnityEngine;

namespace Sources.Lighting {
	public class EmitLight : MonoBehaviour {
		private Transform _transform;
		public float RayLength = 1.0f;
		public LayerMask LayerToIgnore;
		public List<Collider2D> Colliders = new List<Collider2D>();
		private CircleCollider2D _circleCollider;
		private Vector2 _position2D;

		private void Awake() {
			_transform = GetComponent<Transform>();
			_circleCollider = GetComponent<CircleCollider2D>();
		}

		private void FixedUpdate() {
			_circleCollider.radius = RayLength;

			_position2D.x = _transform.position.x;
			_position2D.y = _transform.position.y;
			/*for (var i = 0; i <= 360; i += 10) {
				var quat = Quaternion.AngleAxis(i, _transform.forward);
				var hit = Physics2D.Raycast(_transform.position, quat * _transform.up, Mathf.Infinity, ~LayerToIgnore);
				var color = hit ? Color.green : Color.red;
				if (!hit)
					continue;

				Debug.DrawRay(_transform.position, hit.point, color);
			}*/

			foreach (var coll in Colliders) {
				var hit = Physics2D.Raycast(_transform.position, coll.transform.position - _transform.position, Mathf.Infinity, ~LayerToIgnore);
				Debug.DrawRay(_transform.position, hit.point - _position2D);
			}
		}

		private void OnTriggerEnter2D(Collider2D collider) {
			Colliders.Add(collider);
		}

		private void OnTriggerExit2D(Collider2D collider) {
			Colliders.Remove(collider);
		}
	}
}
