﻿using System;
using System.Collections;
using Assets.Sources.Misc;
using UnityEngine;
using Pathfinding;
using Sources.Controllable;
using UnityEngine.SceneManagement;

namespace Sources.AI {
	public class MinoAI : MonoBehaviour {
		private PlayerHP _player;
		private Animator _animator;
		private AILerp _aiLerp;
		private float _oldSpeed;
		private ControllableMoveHandler.Settings _controllableSettings;
		private float _oldPlayerSpeed;
		public bool CanAttack = true;
		public int HP = 5;
		public FXFade FXFade;


		private void Awake() {
			_aiLerp = GetComponent<AILerp>();
			_aiLerp.target = GameObject.Find("Controllable").GetComponent<Transform>();
			_oldSpeed = _aiLerp.speed;
			_player = GameObject.Find("Controllable").GetComponent<PlayerHP>();
			_controllableSettings = _player.GetComponent<ControllableInstaller>().ControllableMoveHandlerSettings;
			_oldPlayerSpeed = _controllableSettings.PlayerSpeed;
			_animator = GetComponent<Animator>();
		}

		private void OnTriggerEnter2D(Collider2D collider) {
			if (!CanAttack)
				return;
			if (collider.name != "Controllable")
				return;

			CanAttack = false;
			_animator.SetTrigger("Attack");
			_controllableSettings.PlayerSpeed *= 0.5f;
			_aiLerp.speed *= 1.5f;
			_aiLerp.canSearch = false;
		}

		private void Attack() {
			_player.HP -= 1;
			_player.ShowFX();

			if (_player.HP == 0) {
				StartCoroutine(GameOver());
			}
			StartCoroutine(ResetAttack());

		}

		private IEnumerator GameOver() {
			var cameraFader = GameObject.Find("Main Camera").GetComponent<ScreenFader>();
			cameraFader.FadeToBlack();
			yield return new WaitForSeconds(cameraFader.FadeSpeed);
			SceneManager.LoadScene("GameOver");
		}

		private IEnumerator ResetAttack() {
			yield return new WaitForSeconds(0.25f);
			_aiLerp.canMove = false;
			_controllableSettings.PlayerSpeed = _oldPlayerSpeed;
			yield return new WaitForSeconds(2);
			CanAttack = true;
			_aiLerp.canSearch = true;
			_aiLerp.canMove = true;
			_aiLerp.speed = _oldSpeed;
		}
	}
}