﻿using UnityEngine;
using Zenject;
using Sources.Controllable;
using System;
using DG.Tweening;

namespace Sources.Camera {
	public class MainCamera : IInitializable, IFixedTickable, ILateTickable {
		private readonly PixelPerfectCamera _pixelPerfectCamera;
		private readonly Settings _settings;
		private readonly UnityEngine.Camera _camera;
		private readonly Transform _cameraTransform;
		private Vector3 _cameraPositionRounded;

		public MainCamera(PixelPerfectCamera pixelPerfectCamera, UnityEngine.Camera camera,
						  Settings settings) {
			_pixelPerfectCamera = pixelPerfectCamera;
			_camera = camera;
			_settings = settings;
			_cameraTransform = _camera.GetComponent<Transform>();
		}

		public void Initialize() {
			if (_settings.UseFixedHeight)
				_pixelPerfectCamera.ScreenHeight = _settings.FixedHeight;
			_camera.orthographicSize = _pixelPerfectCamera.GetOrthographicSize();

			_cameraTransform.DOMoveX(_settings.Target.position.x, 0.01f);
			_cameraTransform.DOMoveY(_settings.Target.position.y, 0.01f);

			_cameraPositionRounded = _cameraTransform.position;
		}

		public void FixedTick() {
			if (_settings.Debug) {
				Debug.DrawLine(_camera.transform.position, _settings.Target.position, Color.red);
			}

			_cameraTransform.DOMoveX(_settings.Target.position.x, _settings.FollowSpeed);
			_cameraTransform.DOMoveY(_settings.Target.position.y, _settings.FollowSpeed);

		}

		public void LateTick() {
			if (_settings.AutoRoundingValue) {
				_cameraPositionRounded.x = (float) (Math.Round(_cameraTransform.position.x * _settings.CameraRounding, MidpointRounding.ToEven) / _settings.CameraRounding);
				_cameraPositionRounded.y = (float) (Math.Round(_cameraTransform.position.y * _settings.CameraRounding, MidpointRounding.ToEven) / _settings.CameraRounding);
			} else {
				_cameraPositionRounded.x = (float) (Math.Round(_cameraTransform.position.x * _pixelPerfectCamera.GetPixelPerUnit, MidpointRounding.ToEven) / _pixelPerfectCamera.GetPixelPerUnit);
				_cameraPositionRounded.y = (float) (Math.Round(_cameraTransform.position.y * _pixelPerfectCamera.GetPixelPerUnit, MidpointRounding.ToEven) / _pixelPerfectCamera.GetPixelPerUnit);
			}

			_cameraTransform.position = _cameraPositionRounded;
		}

		[Serializable]
		public class Settings {
			public bool Debug;
			public bool CanOpenDoor = true;
			public float FollowSpeed;
			public Transform Target;
			public bool UseFixedHeight = false;
			public int FixedHeight = 1080;
			public bool AutoRoundingValue = true;
			public int CameraRounding = 64;
		}
	}
}