﻿using UnityEngine;

namespace Sources.Camera {
	public interface ICameraProxy {
		float OrthographicSize { get; set; }
		Vector3 Position { get; set; }
	}
}