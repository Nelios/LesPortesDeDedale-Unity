﻿using Sources.Shaders;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace Sources.Camera {
	public class FXController : MonoBehaviour {
		private void Awake() {
			GetComponent<ScreenOverlay>().intensity = PlayerPrefs.GetFloat("GammaCorrection", 2.0f);
			GetComponent<NoiseAndGrain>().enabled = PlayerPrefs.GetInt("PostFX", 1) > 0;
			GetComponent<CRTEffect>().enabled = PlayerPrefs.GetInt("PostFX", 1) > 0;
			GetComponent<VignetteAndChromaticAberration>().enabled = PlayerPrefs.GetInt("PostFX", 1) > 0;
		}
	}
}