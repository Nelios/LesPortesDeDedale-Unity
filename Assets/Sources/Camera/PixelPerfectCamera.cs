﻿using UnityEngine;

namespace Sources.Camera {
	public class PixelPerfectCamera {
		public int PixelPerUnit { private get; set; } = 32;
		public int PixelScale { private get; set; } = 2;
		public int ScreenHeight { private get; set; } = Screen.height;
		public float GetPixelPerUnit => (float) PixelPerUnit * PixelScale;
		public float GetOrthographicSize() => ScreenHeight / GetPixelPerUnit * 0.5f;
	}
}